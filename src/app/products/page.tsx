import Features from "@/components/features";
import React from "react";
// import Products from "@/components/products";
import { ProductType } from "@/interface";
import Product from "@/components/products";
import Cta from "@/components/Cta";

const Products = async () => {
  const res = await fetch("https://fakestoreapi.com/products");
  const products: ProductType[] = await res.json();
  return (
    <div>
      <Features />
      <main className="min-h-screen max max-w-7xl mx-auto px-8 xl:px-0 mt-52">
        <section className="flex flex-col space-y-12">
          <h1 className="text-5xl font-bold text-center">SAMMI SHOP DEALS</h1>
          <div className="grid grid-cols-1 gap-y-10 gap-x-6 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
            {products.map((product) => (
              <Product key={product.id} product={product} />
            ))}
          </div>
        </section>
        <Cta />
      </main>
    </div>
  );
};

export default Products;
