import React from "react";
import Link from "next/link";
import Image from "next/image";
const Navbar = () => {
  return (
    <header className="flex items-center px-4 md:px-12 py-2 justify-between fixed top-0 w-full z-50 shadow  bg-white">
      <Link href={"/ "} className="flex items-center gap-2">
        <Image src={"/favicon.svg"} alt={"logo"} width={60} height={20} />
        <p className="font-bold text-3xl">Sammi</p>
      </Link>
      <div className="flex items-center space-x-2.5 text-sm">
        <nav className="md:ml-auto flex flex-wrap items-center text-base justify-center">
          <Link href={"/"} className="mr-5 hover:text-gray-900">
            Home
          </Link>
          <Link href={"/products"} className="mr-5 hover:text-gray-900">
            All products
          </Link>
          <Link href={"/contact"} className="mr-5 hover:text-gray-900">
            Contact Us
          </Link>
        </nav>
        <Link href={"/shopping-cart"}>
          <button className="button bg-blue-600 text-white border-transparent hover:border-blue-600 hover:bg-transparent hover:text-black ">
            My bag
          </button>
        </Link>
      </div>
    </header>
  );
};

export default Navbar;
